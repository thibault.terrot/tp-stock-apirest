
<div id="recherche">
    <h2><i class="fas fa-search"></i> Rechercher un produit:</h2>
    <br/>
    <?php 
        if(isset($tab_val)){
            echo "<table class='table table-striped'><tr><th>Référence</th><th>Lieu de stockage</th><th>Désignation</th><th>Image</th><th>Prix d'achat</th><th>Quantité</th><th>Zone de stockage</th></tr>";
            $cpt = 0;
            
            foreach($tab_val as $elem){
                echo "<tr>";
                foreach($elem as $e){
                    echo "<td>";
                    echo $e;
                    echo "</td>";
                }
                echo "</tr>";
            }
            echo '</table>';
        }
    ?>
    <br/>
    <form name="search_product" method="post" action="recherche_produit.php">
        <label for="select-field">Rechercher dans: </label>
        <select name="field" id="select-field">
            <option value="designation">Désignation</option>
            <option value="lieu">Lieu de stockage</option>
            <option value="zone">Zone de stockage</option>
            <option value="quantite">Quantité</option>
            <option value="prix_achat">Prix d'achat</option>
            <option value="photo">Photo</option>
        </select>
        <br/><br/>
        <input name="input" type="text"/>
        <button id="btn_add_prod" class="btn btn-primary">Chercher <i class="fas fa-search"></i></button>
    </form>
    <br/>
</div>

                
