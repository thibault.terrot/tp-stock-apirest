<?php

    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit/produit'.$_POST['id'].'.json';
    $ch = curl_init();

    if(isset($_POST['photo'])) $photo = $_POST['photo'];
    else $photo = "src/img/default_product.png";

    if(isset($_FILES['img_modif']) && $_FILES['img_modif']['size'] != 0){
        $targetdir = '../src/img/';
        $photo = $targetdir.basename($_FILES['img_modif']['name']);
        move_uploaded_file($_FILES['img_modif']['tmp_name'], $photo);  # On importe la photo dans src/img/
        $photo = substr($photo,3);
    }

    $data = array(
        "name" => $_POST['designation'],
        "lieu" => $_POST['lieu_stockage'],
        "photo" => $photo,
        "price" => $_POST['prix'],
        "quantity" => $_POST['qty'],
        "zone" => $_POST['zone_stockage'],
    );

    $json = json_encode( $data );

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);
    header('Location: ../index.php');

?>