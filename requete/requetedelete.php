<?php
//$url = 'http://127.0.0.1/AFIP/API-REST/gettousproduits.php?id=1';
$url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit/';

// supprimer le produit

if(!empty($_GET["id"]))
{
    // Récupérer un seul produit
    $id = intval($_GET["id"]);
    $url = $url."produit".$id.".json";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    var_dump($response);
    curl_close($ch);

    $message = "Suppression du produit effectué avec succès";
}
else
{
    $message = "Impossible de supprimer";
}
header('Location: ../index.php');

?>