<?php
    require("../getAll.php");

    $new_id = getNewId();

    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit/produit'.$new_id.'.json';
    $ch = curl_init();


    $photo = "src/img/default_product.png";

    if(isset($_FILES['img_modif']) && $_FILES['img_modif']['size'] != 0){
        $targetdir = '../src/img/';
        $photo = $targetdir.basename($_FILES['img_modif']['name']);
        move_uploaded_file($_FILES['img_modif']['tmp_name'], $photo);  # On importe la photo dans src/img/
        $photo = substr($photo,3);
    }

    $data = array(
        "id" => $new_id,
        "name" => $_POST['designation'],
        "photo" => $photo,
        "lieu" => $_POST['lieu_stockage'],
        "price" => $_POST['prix'],
        "quantity" => $_POST['qty'],
        "zone" => $_POST['zone_stockage'],
    );

    $json = json_encode( $data );

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json );

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    header('Location: ../index.php');
?>