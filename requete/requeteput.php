<?php
//URL cible
//$url = 'http://127.0.0.1/Capsule_Php/code/2_Avance/api-rest/gettousproduits.php?id=1';
$url = 'http://127.0.0.1/AFIP/API-REST/produits.php/1';

// modifie le produit 1
$data = array('name' => 'MAC', 'description' => 'Ordinateur portable', 'price' => '10000', 'category' => '2');
// Initialisez une session CURL : 
$ch = curl_init($url);
// Récupérer le contenu de la page
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//On defini le type de requete ici
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
//Désactiver la vérification du certificat si le site distant utilise HTTPS
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

//On cree la requete qui va envoyer nos données HTTP
curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));


//Exécutez la requête 
$response = curl_exec($ch);

echo "la réponse : ";
var_dump($response);

if (!$response) 
{
    return false;
}
?>

