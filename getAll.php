<?php

function getAllProducts()
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $products = json_decode($response);
    return $products;
}

function getProduct(int $id)
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit/produit'.$id.'.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $product = json_decode($response);
    return $product;
}
function getProductByDesignation(String $designation)
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $Allproduct = json_decode($response);
    $product = array();
    foreach($Allproduct as $key => $row){
        if($row->name == $designation) array_push($product, $row);
    }
    return $product;
}

function getProductByLieu(String $lieu)
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $Allproduct = json_decode($response);
    $product = array();
    foreach($Allproduct as $key => $row){
        if($row->lieu == $lieu) array_push($product, $row);
    }
    return $product;
}

function getProductByZone(String $zone)
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $Allproduct = json_decode($response);
    $product = array();
    foreach($Allproduct as $key => $row){
        if($row->zone == $zone) array_push($product, $row);
    }
    return $product;
}

function getProductByQty(String $quantity)
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $Allproduct = json_decode($response);
    $product = array();
    foreach($Allproduct as $key => $row){
        if($row->quantity == $quantity) array_push($product, $row);
    }
    return $product;
}

function getProductByPrice(String $price)
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $Allproduct = json_decode($response);
    $product = array();
    foreach($Allproduct as $key => $row){
        if($row->price == $price) array_push($product, $row);
    }
    return $product;
}

function getNewId()
{
    $url = 'https://api-rest-f55d9-default-rtdb.firebaseio.com/produit.json';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    $products = json_decode($response);
    $newId = 0;
    foreach($products as $key => $row){
        if($row->id > $newId) $newId = $row->id;
    }
    return $newId +1;
}

?>