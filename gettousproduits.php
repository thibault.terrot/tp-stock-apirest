<?php
	// Connect to database
	include("db_connect.php");
	$request_method = $_SERVER["REQUEST_METHOD"];
//$_SERVER nous permet de savoir quelle est la méthode de la requete envoyée au serveur : lecture, ajout, modification ou suppression.
switch($request_method)
    {
      case 'GET':
        if(!empty($_GET["id"]))
        {
          // Récupérer un seul produit
          $id = intval($_GET["id"]);
          getProduct($id);
        }
        else
        {
          // Récupérer tous les produits
          getProducts();
        }
        break;
        case 'POST':
			// Ajouter un produit
			AddProduct();
      break;
      case 'PUT':
        // Modifier un produit
        $id = intval($_GET["id"]);
        updateProduct($id);
        break;
        case 'DELETE':
          // Supprimer un produit
          $id = intval($_GET["id"]);
          deleteProduct($id);
          break;
      default:
        // Requête invalide
        header("HTTP/1.0 405 Method Not Allowed");
        break;
    }

	function getProducts()
{
    global $conn;
    $query = "SELECT * FROM apirestproduit";
    $response = array();
    $result = mysqli_query($conn, $query);
    while($row = mysqli_fetch_array($result))
    {
        $response[] = $row;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);
}

function getProduct($id=0)
{
    global $conn;
    $query = "SELECT * FROM apirestproduit";
    if($id != 0)
    {
        $query .= " WHERE id=".$id." LIMIT 1";
    }
    $response = array();
    $result = mysqli_query($conn, $query);
    while($row = mysqli_fetch_array($result))
    {
        $response[] = $row;
    }
    header('Content-Type: application/json');
    echo json_encode($response, JSON_PRETTY_PRINT);
}

function AddProduct()
	{
    global $conn;   
    $name = $_POST["name"];
		$description = $_POST["description"];
		$price = $_POST["price"];
		$category = $_POST["category"];
		$created = date('Y-m-d H:i:s');
		$modified = date('Y-m-d H:i:s');
		echo $query="INSERT INTO apirestproduit(name, description, price, category_id, created, modified) VALUES('".$name."', '".$description."', '".$price."', '".$category."', '".$created."', '".$modified."')";
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Produit ajouté avec succès.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'ERREUR!.'. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
  }
  
  function updateProduct($id)
  {
    global $conn;
    $_PUT = array(); //tableau qui va contenir les données reçues
    parse_str(file_get_contents('php://input'), $_PUT);
    $name = $_PUT["name"];
    $description = $_PUT["description"];
    $price = $_PUT["price"];
    $category = $_PUT["category"];
    $modified = date('Y-m-d H:i:s');
    //construire la requête SQL
    $query="UPDATE apirestproduit SET name='".$name."', description='".$description."', price='".$price."', category_id='".$category."', modified='".$modified."' WHERE id=".$id;
    
    if(mysqli_query($conn, $query))
    {
      $response=array(
        'status' => 1,
        'status_message' =>'Produit mis a jour avec succes.'
      );
    }
    else
    {
      $response=array(
        'status' => 0,
        'status_message' =>'Echec de la mise a jour de produit. '. mysqli_error($conn)
      );
      
    }
    
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function deleteProduct($id)
  {
    global $conn;
    $query = "DELETE FROM apirestproduit WHERE id=".$id;
    $res=mysqli_query($conn, $query);
    if($res)//si la requete s'est bien executée 
    {
      $nblignestraitees=mysqli_affected_rows($conn);//si vaut 1 on a supprimé une ligne et donc ça as bien marché
      if ($nblignestraitees) {
        $response=array(
          'status' => 1,
          'status_message' =>' !!! Produit supprimé avec succès.'
        );
      } else {
        $response=array(
          'status' => 1,//on laisse a 1 mais on pourrait définir une autre valeur selons les traitements que l'on souhaite effectués après
          'status_message' =>' !!! Aucun produit supprimé'
        );
      }
      
    }
    else
    {
      $response=array(
        'status' => 0,
        'status_message' =>'La suppression du produit a echoué. '. mysqli_error($conn)
      );
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }

