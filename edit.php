<?php
    include "header.php";
    include "menu_stock.php";

    if(!empty($_GET["id"]))
    {
        $id = intval($_GET["id"]);
        $produit = getProduct($id);

        echo "
        </br>
        <h3 class='text-center'>Modification d'un produit:</h3></br>
        <form enctype='multipart/form-data' method='post' action='requete/requeteedit.php'>

            <table class=\"table table-stripped\">
                <tr>
                    <th>ID</th>
                    <th>Photo</th>
                    <th>Désignation</th>
                    <th>Lieu</th>
                    <th>Zone</th>
                    <th>Quantité</th>
                    <th>Prix</th>
                </tr>
                <tr onclick=\"click_line(this)\" id=\"click\">
                    <td><input name='id' type='text' value=".$produit->id." readonly></input></td>
                    <td>
                        <img src='".$produit->photo."' width=\"80px\" height=\"80px\"></img>
                        <br/><br/>
                        <input style='display:none;' name='photo' value='".$produit->photo."' readonly></input>
                        <input type='file' name='img_modif'/><br/>                        
                    </td>
                    <td><input name='designation' type='text' placeholder='Désignation' value=".$produit->name." required/></td>
                    <td><input name='lieu_stockage' type='text' placeholder='Lieu de stockage' value=".$produit->lieu." required/></td>
                    <td><input name='zone_stockage' type='text' placeholder='Zone de stockage' value=".$produit->zone." required/></td>
                    <td><input name='qty' type='text' placeholder='Quantité' value=".$produit->quantity." required/></td>
                    <td><input name='prix' type='text' placeholder='Prix' value=".$produit->price." required/></td>
                    
                </tr>
            </table>
            <br/>
            <div class='text-center'>
                <button type='sumbit' class='btn btn-primary'>Confirmer</button>
                <a href='./index.php' class='btn btn-danger'>Annuler</a>
            </div>
        </form>
        ";
    }
?>