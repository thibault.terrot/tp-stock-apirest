<?php    
    require("getAll.php");
    
    if ($_POST["input"]) {
        $input = $_POST["input"];
    }

    if ($_POST["field"]) {
        $field = $_POST["field"];
    } 
    
    $tab_val = False;
    if(isset($input) && isset($field) && $field=="designation") $tab_val = getProductByDesignation($input);
    if(isset($input) && isset($field) && $field=="lieu") $tab_val = getProductByLieu($input);
    if(isset($input) && isset($field) && $field=="zone") $tab_val = getProductByZone($input);
    if(isset($input) && isset($field) && $field=="quantite") $tab_val = getProductByQty($input);
    if(isset($input) && isset($field) && $field=="prix_achat") $tab_val = getProductByPrice($input);

    require_once('index.php');
?>