<?php
    

   
echo "<div id=\"liste\">";

if(isset($message)) echo"<span class='alert alert-success'></span>";

echo"
<h2><i class=\"fas fa-cubes\"></i> Liste des produits:</h2><br/>
<table class=\"table table-striped\">
    <tr>
      <th>Photo</th>
      <th>Référence(id)</th>
      <th>Désignation(name)</th>
      <th>Lieu de stockage</th>
      <th>Zone de stockage</th>
      <th>Quantité</th>
      <th>Prix d'achat</th>
      <th></th>
      <th></th>
    </tr>";

$products = getAllProducts();
foreach($products as $key => $row){
    echo "<tr onclick=\"click_line(this)\" id=\"click\">
            <td>
                <img src='".$row->photo."' width=\"80px\" height=\"80px\"> 
                </img> 
            </td>
            <td>".$row->id."</td>
            <td>".$row->name."</td>
            <td>".$row->lieu."</td>
            <td>".$row->zone."</td>
            <td>".$row->quantity."</td>
            <td>".$row->price."</td>
    
            <td title=\"Supprimer\" onclick=\"delete_line(this)\" id=\"delete\">
                <a href='./requete/requetedelete.php?id=".$row->id."' name=\"delete_prod\" class=\"suppr fa fa-trash\"></button>
            </td>
            <td title=\"Editer\" id=\"edit\">
                <a href='./edit.php?id=".$row->id."' name=\"edit_prod\" class=\"edit fa fa-edit\"></button>
            </td>
        </tr>";
}
echo "</table></div>";
        
   
?>