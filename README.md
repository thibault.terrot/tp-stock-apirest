API REST Firebase - Terrot Thibault

Il est nécessaire de disposer d'un compte pour pouvoir se connecter à la console de firebase. Une fois ceci fait, creer un nouveau projet avec une "realtime database".

Lorsque la realtime database est crée il va être nécessaire d'importer le fichier "BDD-Firebase.json" (present à la racine du projet) en cliquant sur les 3points en haut à droite de la bdd puis "importer au format JSON". 
Une fois la BDD importée, vous devriez voir apparaitre les produits dans la BDD.

Pour relier votre nouvelle BDD firebase avec le projet API REST, il va falloir changer le fichier dans le dossier "secret" à la racine du projet.
Ce fichier permet de se connecter à Firebase, pour générer le votre:
    - Aller dans parametre du projet (la roue dentée)
    - Compte de service
    - Générer une nouvelle clé privée

Un fichier sera alors téléchargé sur votre PC, il faudrat ensuite le remplacer par celui se trouvant dans le dossier "secret".
Dans le fichier "header" à la ligne 7, il faudrat changer le nom du fichier par le votre.
            $factory = (new Factory)->withServiceAccount(__DIR__.'./secret/VOTRE NOM DE FICHIER.json');
Si toutefois une erreur de type "_Fatal error: Uncaught Kreait\Firebase\Exception\Database\DatabaseNotFound_" apparait apres avoir remplacé le fichier, tentez de rajouter "-default-rtdb" à la ligne 3 ("project_id") derriere le nom de votre projet.

        api-rest-xxxxx => api-rest-xxxxx-default-rtdb



