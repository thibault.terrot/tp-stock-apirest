
<div id="ajout">
    <h2><i class="fas fa-plus"></i> Ajouter un produit:</h2>
    <br/>
    <span class="alert alert-info">Il est possible de cliquer sur une ligne de la liste pour ajouter plus rapidement un produit.</span> 
    <br/>
    <br/>
    <form enctype="multipart/form-data" method='POST' action='requete/requeteadd.php'>
        <table class="table table-stripped">
            <tr>
                <th>Photo</th>
                <th>Désignation</th>
                <th>Lieu</th>
                <th>Zone</th>
                <th>Quantité</th>
                <th colspan="2">Prix</th>
            </tr>
            <tr>
                <td>
                    <input type="file" name="img_modif"/><br/>
                </td>
                <td><input name='designation' type='text' placeholder='Désignation' required/></td>
                <td><input name='lieu_stockage' type='text' placeholder='Lieu de stockage' required/></td>
                <td><input name='zone_stockage' type='text' placeholder='Zone de stockage' required/></td>
                <td><input name='qty' type='text' placeholder='Quantité' required/></td>
                <td><input name='prix' type='text' placeholder='Prix' required/></td>
                <td><button type='sumbit' class='btn btn-primary'><span class="fa fa-plus"></span> Ajout produit</button></td>
            </tr>
        </table>
    </form>
</div>