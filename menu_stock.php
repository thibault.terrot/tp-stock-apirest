<html>
    <nav id="main_menu_top" class="navbar navbar-expand-lg navbar-dark bg-dark">                    
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/index.php"><i class="fas fa-home"></i> Accueil</a>
                </li>
                <li class="nav-item">
                    TP- API REST - Firebase
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://console.firebase.google.com/u/0/project/api-rest-f55d9/database/api-rest-f55d9-default-rtdb/data"><i class="fas fa-database"></i> Firebase</a>
                </li>
            </ul>
    </nav>
</html>