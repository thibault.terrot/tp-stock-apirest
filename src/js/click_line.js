
//Permet de sélectionner une ligne et de la remplir dans les champs pour l'ajout/modification
function click_line(obj){
    var text_line = ($(obj).text()).trim();
    var tab_line = text_line.split("\n");
    $('input[name="designation"]').val(tab_line[1].trim());
    $('input[name="lieu_stockage"]').val(tab_line[2].trim());
    $('input[name="zone_stockage"]').val(tab_line[3].trim());
    $('input[name="qty"]').val(parseInt(tab_line[4].trim()));
    $('input[name="prix"]').val(tab_line[5].trim());
}


//Permet de supprimer une ligne (utilisation d'ajax pour appeler une fonction php)
function delete_line(obj){
    var text_line = ($(obj).text()).trim();
    var tab_line = text_line.split("\n");

    jQuery.ajax({
        type: "POST",
        url: 'del_produit.php',
        data: {line: $(obj).parent().text()},
    });


    $(obj).parent().remove();
   
    
}


  